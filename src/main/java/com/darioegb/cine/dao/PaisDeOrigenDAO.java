/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.darioegb.cine.dao;

import com.darioegb.cine.model.PaisDeOrigen;
import java.util.List;

/**
 *
 * @author dgonzalez
 */
public interface PaisDeOrigenDAO {
    
    public PaisDeOrigen getPaisDeOrigen(int paisDeOrigenId);
    public List<PaisDeOrigen> getAllPaisDeOrigen();
    public void savePaisDeOrigen(PaisDeOrigen paisDeOrigen);
    public void updatePaisDeOrigen(PaisDeOrigen paisDeOrigen);
    public void deletePaisDeOrigen(int paisDeOrigenId);
}
