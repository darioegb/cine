/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.darioegb.cine.dao;

import com.darioegb.cine.model.PaisDeOrigen;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author dgonzalez
 */
@Repository
public class PaisDeOrigenDAOImpl implements PaisDeOrigenDAO {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public PaisDeOrigen getPaisDeOrigen(int paisDeOrigenId) {
        return (PaisDeOrigen) sessionFactory.getCurrentSession().get(PaisDeOrigen.class, paisDeOrigenId);
    }

    @Override
    public List<PaisDeOrigen> getAllPaisDeOrigen() {
        return sessionFactory.getCurrentSession().createQuery("from PaisDeOrigen").list();
    }

    @Override
    public void savePaisDeOrigen(PaisDeOrigen paisDeOrigen) {
        sessionFactory.getCurrentSession().save(paisDeOrigen);
    }

    @Override
    public void updatePaisDeOrigen(PaisDeOrigen paisDeOrigen) {
        sessionFactory.getCurrentSession().saveOrUpdate(paisDeOrigen);
    }

    @Override
    public void deletePaisDeOrigen(int paisDeOrigenId) {
        PaisDeOrigen paisDeOrigen = getPaisDeOrigen(paisDeOrigenId);
        sessionFactory.getCurrentSession().delete(paisDeOrigen);
    }
    
}
